#!/bin/sh

#export PYTHONPATH=/usr/share/pyspread
#rm __init__.py

mkdir -p /tmp/runtime-pyspreadci
export XDG_RUNTIME_DIR=/tmp/runtime-pyspreadci

py.test-3 -k 'not test_get_header and not test_csv_reader'
